<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\DoctorController;

use App\Http\Controllers\API\AppoinmentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[LoginController::class,'Login']);


Route::middleware('auth:api')->group(function () {
    Route::get('logout', [LoginController::class, 'Logout']);

    Route::group(['prefix' => 'doctor', 'as' => 'doctor.'], function () {
        Route::post('store', [DoctorController::class, 'store'])->name('store');
        Route::post('update', [DoctorController::class, 'update'])->name('update');
    });

    Route::group(['prefix' => 'appoinment'], function () {
        Route::post('store', [AppoinmentController::class, 'store'])->name('store');
        Route::post('update', [AppoinmentController::class, 'update'])->name('update');
        Route::get('show/{id}', [AppoinmentController::class, 'show'])->name('show');
    });
});
