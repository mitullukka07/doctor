<?php
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\AppoinmentController;

Route::group(['namespace' => 'Auth'], function () {
    # Login Routes
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('admin/logout', 'LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');
});


Route::group(['prefix'=>'admin'],function(){
    Route::get('profile',[AdminController::class,'profile'])->name('profile');
    Route::post('profile',[AdminController::class,'updateprofile']);
    Route::get('totalAppoinment',[AdminController::class,'totalAppoinment']);
});


Route::group(['prefix'=>'doctor'],function(){
    Route::get('create',[DoctorController::class,'create'])->name('property.create');
    Route::get('index',[DoctorController::class,'index'])->name('property.index');
    Route::post('store',[DoctorController::class,'store'])->name('property.store');
    Route::get('edit/{id}',[DoctorController::class,'edit'])->name('property.edit');
    Route::post('update',[DoctorController::class,'update'])->name('property.update');
    Route::post('destroy',[DoctorController::class,'destroy'])->name('destroy');    
    Route::get('show/{id}',[DoctorController::class,'show'])->name('property.show');
    
});

Route::group(['prefix'=>'appoinment'],function(){
    Route::get('create',[AppoinmentController::class,'create'])->name('create');
    Route::get('index',[AppoinmentController::class,'index'])->name('index');
    Route::post('store',[AppoinmentController::class,'store'])->name('store');
    Route::get('show/{id}',[AppoinmentController::class,'show'])->name('show');
});

?>