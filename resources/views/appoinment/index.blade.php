    @extends('layouts.admin.master')
    @section('content')
    @section('title','Appoinment')
    @push('css')
    <style>
        .allerror {
            color: red
        }
    </style>
    @endpush
    <!-- Loader -->
    <div id="layout-wrapper">
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">Appoinment List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item">Appoinment</li>
                                    <li class="breadcrumb-item active">Appoinment List</li>
                                </ol>
                            </div>

                        </div>
                            
                        </div>
                    </div>

                    <div id="myModalshowappoinment" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">View Appoinment</h4>
                                    <button type="button" class="close" data-bs-dismiss="modal">&times;</button>
                                </div>
                                <table class="table table-borderd">
                                    <div class="modal-body">
                                        <form id="showform" enctype="multipart/form-data">
                                            @csrf
                                            <tr>
                                                <th><b>Name</b></th>
                                                <td><span id="set_name"></span></td>
                                            </tr>
                                            <tr>
                                                <th><b>DoctorName</b></th>
                                                <td><span id="set_doctor"></span></td>
                                            </tr>
                                            <tr>
                                                <th><b>StartTime</b></th>
                                                <td><span id="set_start"></span></td>
                                            </tr>
                                            <tr>
                                                <th><b>EndTime</b></th>
                                                <td><span id="set_end"></span></td>
                                            </tr>
                                            <tr>
                                                <th><b>DateTime</b></th>
                                                <td><span id="set_datetime"></span></td>
                                            </tr>
                                    </div>
                                </table>
                                <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.create')}}" id="update" name="update" class="btn btn-primary"><i class="fa fa-plus"></i>Add Appoinment</a>
                                    {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('js')
    {!! $dataTable->scripts() !!}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        //View data
        $(document).on("click", '.show', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $("#set_name").text(data.appoinment.name);
                    $("#set_doctor").text(data.name);
                    $("#set_start").text(data.appoinment.start_time);
                    $("#set_end").text(data.appoinment.end_time);
                    $("#set_datetime").text(data.appoinment.datetime);                    
                }
            });
        })
    </script>
    @endpush