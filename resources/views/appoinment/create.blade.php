@extends('layouts.admin.master')
@section('title','Create')
@section('content')
<!-- Begin page -->
<div id="layout-wrapper">
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">Appoinment</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Doctor</a></li>
                                    <li class="breadcrumb-item active">Appoinment</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <!-- end row -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">

                                <form class="custom-validation" id="myform" method="post">
                                    @csrf
                                    @php
                                    $doctor = DB::table('doctors')->get();
                                    @endphp

                                    <div class="mb-3">
                                        <label for="cars">DoctorName</label>
                                        <select class="form-control" name="doctorname" id="doctorname">
                                            <option>-------Select Doctor--------</option>
                                            @foreach($doctor as $doc)
                                            <option value="{{$doc->id}}">{{$doc->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Enter Name" />
                                        <span class="text-danger" id="nameError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="Enter Email" />
                                        <span class="text-danger" id="emailError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <div class="col-lg-12">
                                            <h4 class="card-title">Address</h4>
                                            <div id="classic-editor">
                                                <textarea class="form-control" id="address" name="address"></textarea>
                                            </div>
                                            <span class="text-danger" id="addressError"></span></br>
                                        </div>
                                    </div>
                                    <!-- 

                                    <div class="mb-3">
                                        <label class="form-label">FromTime</label>
                                        <input type="text" class="form-control timepicker" name="fromtime" />
                                        <span class="text-danger" id="fromError"></span></br>
                                    </div> -->

                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="start_time" class="col-form-label">Start Time</label>
                                            <div class='input-group date' id='starttime'>
                                                <input type='text' class="form-control" name="fromtime" id="Start_Time" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                            <span class="text-danger" id="fromError"></span></br>
                                        </div>
                                    </div>

                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="end_time" class="col-form-label">End Time</label>
                                            <div class='input-group date' id='endtime'>
                                                <input type='text' class="form-control" name="totime" id="Start_Time" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                            <span class="text-danger" id="toError"></span></br>
                                        </div>
                                    </div>


                                    <!-- <div class="mb-3">
                                        <label class="form-label">ToTime</label>
                                        <input type="text" class="form-control timepicker" name="totime" />
                                        <span class="text-danger" id="toError"></span></br>
                                    </div> -->

                                    <div class="mb-3">
                                        <label class="form-label">DateTime</label>
                                        <input type="text" class="form-control" id="datepicker" name="datetime" />
                                        <span class="text-danger" id="titleError"></span></br>
                                    </div>


                                    <div>
                                        <div>
                                            <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light me-1">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- container-fluid -->
        </div>
    </div>
</div>
<!-- END layout-wrapper -->
@endsection

@push('js')
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" /> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("admin.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("admin.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $("#nameError").text(result.responseJSON.errors.name);
                    $("#emailError").text(result.responseJSON.errors.email);
                    $("#addressError").text(result.responseJSON.errors.address);
                    $("#fromError").text(result.responseJSON.errors.start_time);
                    $("#toError").text(result.responseJSON.errors.end_time);
                }
            })
        });
    })
</script>
<script>
    ClassicEditor
        .create(document.querySelector('#classic-editor'))
        .catch(error => {
            console.error(error);
        });
</script>
<script type="text/javascript">
    $('.timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '11',
        maxTime: '7:00pm',
        defaultTime: '11',
        startTime: '10:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $(function() {
        $("#datepicker").datepicker({
            startDate: '-0m',
            format: "yyyy/mm/dd"
        });
    });

    function TimePickerCtrl($) {
        var startTime = $('#starttime').datetimepicker({
            format: 'HH:mm a',
        });

        var endTime = $('#endtime').datetimepicker({
            format: 'HH:mm a',            
            minDate: startTime.data("DateTimePicker").date()
        });

        function setMinDate() {
            return endTime
                .data("DateTimePicker").minDate(
                    startTime.data("DateTimePicker").date()
                );
        }

        var bound = false;
        function bindMinEndTimeToStartTime() {
            return bound || startTime.on('dp.change', setMinDate);
        }
        endTime.on('dp.change', () => {
            bindMinEndTimeToStartTime();
            bound = true;
            setMinDate();
        });
    }
    $(document).ready(TimePickerCtrl);
</script>
@endpush