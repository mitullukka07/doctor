@extends('layouts.admin.master')
@section('title','Doctor')
@section('content')
@push('css')
<style>
    .error {
        color: red;
    }
</style>
@endpush
<!-- Begin page -->
<div id="layout-wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">Appoinment</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Doctor</a></li>
                                    <li class="breadcrumb-item active">Doctor List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Doctor Update</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form id="editform" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" />
                                        <span class="text-danger" id="titleError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" />
                                        <span class="text-danger" id="titleError"></span></br>
                                    </div>


                                    <div class="mb-3">
                                        <label class="form-label">Mobile</label>
                                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter mobile" />
                                        <span class="text-danger" id="titleError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <div class="col-lg-12">
                                            <h4 class="card-title">Address</h4>
                                            <div id="classic-editor">
                                                <textarea class="form-control" id="address" name="address"></textarea>
                                            </div>
                                            <span class="text-danger" id="descriptionError"></span></br>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">FromTime</label>
                                        <input type="text" class="form-control timepicker" name="fromtime" />
                                        <span class="text-danger" id="startError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">ToTime</label>
                                        <input type="text" class="form-control timepicker" name="totime" />
                                        <span class="text-danger" id="endError"></span></br>
                                    </div>



                                    <div class="mb-3">
                                        <label class="form-label">Photo</label>
                                        <input type="file" class="form-control" id="image" name="image" />
                                        <img id="doctorimage" width="50px" width="50px">
                                        <span class="text-danger" id="mobileError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Gender</label></br>
                                        <input type="radio" id="male" name="gender" value="1">
                                          <label for="html">Male</label>
                                          <input type="radio" id="female" name="gender" value="2">
                                          <label for="css">Female</label><br>
                                    </div>

                                    <div id="trackingDiv"></div>
                            </div>
                            <input type="hidden" id="id" name="id">
                            </form>
                            <div class="modal-footer">
                                <button typedoctorimage="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" id="update" name="update" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="myModalshow" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">View Doctor</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <table class="table table-border">
                                <div class="modal-body">
                                    <form id="showform" enctype="multipart/form-data">
                                        @csrf
                                        <tr>
                                            <th><b>Name</b></th>
                                            <td><span id="set_name"></span></td>
                                        </tr>

                                        <tr>
                                            <th><b>Email</b></th>
                                            <td><span id="set_email"></span></td>
                                        </tr>

                                        <tr>
                                            <th><b>Mobile</b></th>
                                            <td><span id="set_mobile"></span></td>
                                        </tr>

                                        <tr>
                                            <th><b>Image</b></th>
                                            <td><img src="" id="set_image" width="50px" height="50px"></img></td>
                                        </tr>

                                        <tr>
                                            <th><b>Address</b></th>
                                            <td><span id="set_address"></span></td>
                                        </tr>

                                        <tr>
                                            <th><b>Gender</b></th>
                                            <td><span id="set_gender"></span></td>
                                        </tr>
                                </div>
                            </table>
                            <input type="hidden" id="id" name="id">
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.property.create')}}" id="update" name="update" class="btn btn-primary"><i class="fa fa-plus"></i>Add Doctor</a>
                                {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@endsection
@push('js')
{!! $dataTable->scripts() !!}

<script>
    $(document).on("click", ".btnedit", function(e) {
        e.preventDefault();
        var id = $(this).attr("href");
        $.ajax({
            url: id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#id").val(data.doctor.id);
                $("#name").val(data.doctor.name);
                $("#email").val(data.doctor.email);
                $("#mobile").val(data.doctor.mobile);
                $("#address").val(data.doctor.address);
                $("#doctorimage").attr('src', data.doctor.image);
                if (data.doctor.gender == 1) {
                    $('#male').prop('checked', true);
                } else {
                    $('#female').prop('checked', true);
                }
            },
        });
    });

    //update
    $("#update").click(function(e) {
        e.preventDefault();
        var form = $("#editform")[0];
        var data = new FormData(form);
        $.ajax({
            url: '{{route("admin.property.update")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data) {
                swal({
                    title: 'Updated',
                    text: 'Doctor Updated Succesfully',
                    buttons: ['Cancel', 'Update']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['doctor-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            },
            error: function(data) {
                $.each(data.responseJSON.errors, function(key, value) {
                    $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                });
            }
        });
        $("#myModal").on('hidden.bs.modal', function() {
            $('.error').empty();
        });
    });


    $(document).on("click", ".btndelete", function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('admin.destroy')}}",
            type: 'post',
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                swal({
                    title: 'Delete',
                    text: 'Are you sure delete succesfully ??',
                    buttons: ['Cancel', 'Delete']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['doctor-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            }
        });
    });

    $(document).on("click", '.btnshow', function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#set_name").text(data.doctor.name);
                $("#set_email").text(data.doctor.email);
                $("#set_mobile").text(data.doctor.mobile);
                $("#set_image").attr('src', data.doctor.image);
                $("#set_address").text(data.doctor.address);
                if(data.doctor.gender == '1')
                {
                    $("#set_gender").text('male');
                }else{
                    $("#set_gender").text('female');
                }
            }
        });
    });
</script>

<script type="text/javascript">
    $('.timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '11',
        maxTime: '7:00pm',
        defaultTime: '11',
        startTime: '10:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $(function() {
        $("#datepicker").datepicker({
            startDate: '-0m'
        });
    });
</script>
@endpush