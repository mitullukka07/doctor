<?php

    namespace App\Interfaces;

    interface DoctorRepositoryInterface
    {
        public function store($array);
        public function update($array);
    }

?>