<?php

    namespace App\Interfaces;

    interface LoginRepositoryInterface
    {
        public function Login($array);
        public function Logout();
    }

?>