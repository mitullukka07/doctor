<?php

namespace App\Interfaces;

interface AppoinmentRepositoryInterface
{
    public function store($array);
    public function update($array);
    public function show($id);
}

?>