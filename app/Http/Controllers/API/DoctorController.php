<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Doctor\StoreRequest;
use App\Http\Requests\Doctor\UpdateRequest;
use App\Repository\DoctorRepository;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function __construct(DoctorRepository $doctorRepository)
    {
        $this->doctorRepository = $doctorRepository;
    }

    public function store(StoreRequest $request)
    {
        $store = $this->doctorRepository->store($request);
        return response()->json(['doctor'=>$store]);        
    }

    public function update(UpdateRequest $request)
    {
        $update = $this->doctorRepository->update($request);
        return response()->json(['doctor'=>$update]);        
    }
}
