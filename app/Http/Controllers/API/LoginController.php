<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Repository\LoginRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }

    public function Login(Request $request)
    {
        return $this->loginRepository->login($request);
    }

    public function Logout(Request $request)
    {
        return $this->loginRepository->logout($request);
    }    
}
