<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repository\AdminRepository;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function profile($array)
    {
        $show = $this->appoinmentRepository->profile();
        return response()->json(['appoinment'=>$show]);
    }
}
