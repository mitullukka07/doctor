<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repository\AppoinmentRepository;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class AppoinmentController extends Controller
{
    public function __construct(AppoinmentRepository $appoinmentRepository)
    {
        $this->appoinmentRepository = $appoinmentRepository;
    }

    public function store(Request $request)
    {
        $store = $this->appoinmentRepository->store($request);
        return response()->json(['appoinment'=>$store]);        
    }

    public function show($id)
    {
        $show = $this->appoinmentRepository->show($id);
        return response()->json(['appoinment'=>$show]);
    }
    public function update(Request $request)
    {
        $update = $this->appoinmentRepository->update($request);
        return response()->json(['appoinment'=>$update]);        
    }
}
