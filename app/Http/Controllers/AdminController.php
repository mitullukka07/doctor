<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Appoinment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function profile()
    {
        $admin = Auth::guard('admin')->user();
        return view('admin.profile',compact('admin'));
    }

    public function updateprofile(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $admin->name = $request->name;
        if($admin->save())
        {
            return redirect()->back();
        }
    }

    public function totalAppoinment()
    {   
        $posts = Appoinment::whereDate('created_at', Carbon::today())->count();
        return view('admin.dasboard',compact('posts'));
    }
}
