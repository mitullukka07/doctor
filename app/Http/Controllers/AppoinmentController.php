<?php

namespace App\Http\Controllers;

use App\DataTables\AppoinmentDataTable;
use App\Http\Requests\Appoinment\StoreRequest;
use App\Models\Appoinment;
use App\Repository\AppoinmentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AppoinmentController extends Controller
{
    public function __construct(AppoinmentRepository $appoinmentRepository)
    {
        $this->appoinmentRepository = $appoinmentRepository;
    }
    
    public function index(AppoinmentDataTable $appoinmentDataTable)
    {       
        return $appoinmentDataTable->render('appoinment.index');        
    }

    public function create()
    {
        return view('appoinment.create');
    }
  
    public function store(StoreRequest $request)
    {        
        $store = $this->appoinmentRepository->store($request);
        return response()->json(['appoinment'=>$store]);  
    }

    public function show($id)
    {
        $show = $this->appoinmentRepository->show($id);
        $name = $show->doctor_name->name;
        return response()->json(['appoinment'=>$show,'name'=>$name]);
    }

    public function edit(Appoinment $appoinment)
    {
        
    }

    public function update(Request $request, Appoinment $appoinment)
    {
        $update = $this->appoinmentRepository->update($request);
        return response()->json(['appoinment'=>$update]);
    }

    public function destroy(Appoinment $appoinment)
    {
        
    }
}
