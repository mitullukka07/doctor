<?php

namespace App\Http\Controllers;

use App\DataTables\DoctorDataTable;
use App\Http\Requests\Doctor\StoreRequest;
use App\Http\Requests\Doctor\UpdateRequest;
use App\Models\Doctor;
use App\Repository\DoctorRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class DoctorController extends Controller
{

    public function __construct(DoctorRepository $doctorRepository)
    {
        $this->doctorRepository = $doctorRepository;
    }
    
    public function index(DoctorDataTable $docotrDataTable)
    {
        return $docotrDataTable->render('admin.index');
    }
    
    public function create()
    {
        return view('admin.create');
    }
    
    public function store(StoreRequest $request)
    {
        return $this->doctorRepository->store($request);
    }

    public function show($id)
    {
        $doctor = Doctor::find($id);
        return response()->json(['doctor'=>$doctor]);
    }

    public function edit($id)
    {
        $doctor = Doctor::find($id);
                    
        return response()->json(['doctor'=>$doctor]);
    }

    public function update(UpdateRequest $request)
    {
        $update = $this->doctorRepository->update($request);
        return response()->json(['doctor'=>$update]);
    }

    public function destroy(Request $request)
    {
        $doctor = Doctor::where('id',$request->id)->delete();
        return redirect()->route('admin.property.index')->with('delete',"Delete Successfully");
    }
}
