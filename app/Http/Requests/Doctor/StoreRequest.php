<?php

namespace App\Http\Requests\Doctor;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'mobile' => 'required',
            'email' => 'required|email|unique:doctors',
            'address' => 'required',           
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter name',
            'mobile.required' => 'Please enter mobile',
            'email.required' => 'Please enter mobile',
            'address.required' => 'Please enter address',
        ];
    }
}
