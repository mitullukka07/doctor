<?php

if (!function_exists('uploadFile')) {
    function uploadFile($image, $dir)
    {
        if ($image) {
            $destinationPath =  storage_path('app/public') . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR;
            $media_image = $image->hashName();
            $image->move($destinationPath, $media_image);
            return $media_image;
        }
    }
}


?>