<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appoinment extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','doctor','datetime','start_time','end_time'
    ];

    public function doctor_name()
    {
        return $this->belongsTo(Doctor::class, 'doctor', 'id')->select('name');
    }
}
