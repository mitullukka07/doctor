<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Doctor extends Model
{
    use HasFactory,HasApiTokens;
    use SoftDeletes;
    
    protected $fillable = [
        'name','email','mobile','address','gender','image','start_time','end_time','Datetime'
    ];

    public function getImageAttribute($value)
    {
        return $value ? asset('storage/UserProfilepicture' . '/' . $value) : NULL;
    }
}
