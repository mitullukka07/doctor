<?php
namespace App\Repository;

use App\Interfaces\LoginRepositoryInterface;
use App\Jobs\SendMail;
use App\Models\Admin;
use App\Models\Login;
use App\Models\PrivacyPolicy;
use App\Models\Register;
use App\Models\TermsCondition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class LoginRepository implements LoginRepositoryInterface
{
    public function Login($array)
    {
        $email = $array->email;
        $password = $array->password;
        $user = Admin::where('email', $email)->first();

        if ($user && Hash::check($password, $user->password)) {
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function Logout()
    {
        $token = Auth::user()->token();
        $token->revoke();
        return response(['message' => 'You have been successfully logged out.'], 200);
    }
}
?>

 