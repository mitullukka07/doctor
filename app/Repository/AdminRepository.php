<?php
namespace App\Repository;

use App\Interfaces\AdminRepositoryInterface;
use App\Interfaces\DoctorRepositoryInterface;
use App\Models\Doctor;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Support\Facades\Auth;

class AdminRepository implements AdminRepositoryInterface
{
    public function profile($array)
    {
        $admin = Auth::guard('admin')->user();
        return $admin;  
    }   
}
?>

 