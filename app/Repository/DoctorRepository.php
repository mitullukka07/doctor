<?php
namespace App\Repository;

use App\Interfaces\DoctorRepositoryInterface;
use App\Models\Doctor;
use Facade\FlareClient\Stacktrace\File;

class DoctorRepository implements DoctorRepositoryInterface
{
    public function store($array)
    {
        $file = uploadFile($array->image, 'UserProfilepicture');
        $doctor = new Doctor;
        $doctor->name = $array->name;
        $doctor->email = $array->email;
        $doctor->mobile = $array->mobile;
        $doctor->address = $array->address;
        $doctor->gender = $array->gender;
        $doctor->start_time = $array->fromtime ;
        $doctor->end_time = $array->totime;
        $doctor->Datetime = $array->Datetime;
        $doctor->image = $file;
        $doctor->save();
        return $doctor; 
    }

    public function update($array)
    {    
        $doctor = Doctor::find($array->id);
        $doctor->name = $array->name;
        if ($array->hasFile('image')) { 
            $destination = 'storage/UserProfilepicture/' . $doctor->image;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $array->file('image');
            $name = $file->getClientOriginalName();
            $name = $file->hashName();
            $file->move('storage/UserProfilepicture/', $name);

            $doctor->name = $array->name;
            $doctor->mobile = $array->mobile;
            $doctor->email = $array->email;
            $doctor->address = $array->address;
            $doctor->gender = $array->gender;
            $doctor->start_time = $array->fromtime;
            $doctor->end_time = $array->totime;
            $doctor->datetime = $array->datetime;
            $doctor->image = $name;
            $doctor->update();
        }
        return $doctor;
    }
}
?>

 