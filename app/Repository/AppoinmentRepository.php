<?php
namespace App\Repository;

use App\Interfaces\AppoinmentRepositoryInterface;
use App\Models\Appoinment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class AppoinmentRepository implements AppoinmentRepositoryInterface
{
    public function store($array)
    {
        $appoinment = new Appoinment;
        $appoinment->doctor = $array->doctorname;
        $appoinment->name = $array->name;
        $appoinment->email = $array->email;
        $appoinment->address = $array->address;
        $appoinment->start_time = $array->fromtime;
        $appoinment->end_time = $array->totime;
        $appoinment->datetime = $array->datetime;
        $appoinment->save();

        $data = ['title' => $appoinment];
        $user['to'] = $appoinment->email;
        Mail::send('mails.mail', $data, function ($message) use ($user) {
            $message->to($user['to']);
            $message->subject('Hello');
        });
        return $appoinment;
    }

    public function show($id)
    {
        $appoinment = Appoinment::findOrFail($id);                
        return $appoinment;
    }

    public function update($array)
    {
        $appoinment = Appoinment::find($array->id);
        $appoinment->doctor = $array->doctor;
        $appoinment->name = $array->name;
        $appoinment->email = $array->email;
        $appoinment->address = $array->address;
        $appoinment->start_time = $array->start_time;
        $appoinment->end_time = $array->end_time;
        // $appoinment->datetime = $array->datetime;
        $appoinment->datetime = Carbon::createFromFormat('d F Y - H:i', $array->datetime);
        $appoinment->update();

        $data = ['title' => $appoinment];
        $user['to'] = $appoinment->email;
        Mail::send('mails.mail', $data, function ($message) use ($user) {
            $message->to($user['to']);
            $message->subject('Hello');
        });
        return $appoinment;
    }
}
?>

 