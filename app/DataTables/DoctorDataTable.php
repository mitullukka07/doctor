<?php

namespace App\DataTables;

use App\Models\Doctor;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class DoctorDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $btn = "";
                $btn = '<a href="' . route('admin.property.edit', $data->id) . '" data-id="' . $data->id . '"    data-bs-backdrop="static" data-bs-keyboard="false"  data-bs-toggle="modal" data-bs-target="#myModal" class="edit btn btn-info btn-sm btnedit"><i class="fa fa-edit"></i></a>&nbsp';
                $btn .= '<a href="' . route('admin.destroy', $data->id) . '" data-id="' . $data->id . '"   class="edit btn btn-danger btn-sm btndelete"><i class="fa fa-trash"></i></a>&nbsp ';
                $btn .= '<a href="' . route('admin.property.show', $data->id) . '" data-id="' . $data->id . '" data-bs-backdrop="static" data-bs-keyboard="false" data-bs-toggle="modal" data-bs-target="#myModalshow"  class="edit btn btn-primary btn-sm btnshow"><i class="fa fa-eye"></i></a>&nbsp ';

                return $btn;
            })
            ->editColumn('image', function ($data) {
                return '<img src="' . asset($data->image) . '"   width="40" class="img-rounded" align="center" />';
            })
            ->addColumn('gender', function ($data) {
                if ($data->gender == '1') {
                    return 'Male';
                } else {
                    return 'Female';
                }
            })
            ->rawColumns(['action', 'gedner', 'image'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Doctor $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Doctor $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('doctor-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           
            Column::make('NO')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('mobile'),
            Column::make('email'),
            Column::make('address'),
            Column::make('gender'),
            Column::make('start_time'),
            Column::make('end_time'),
            Column::make('image'),
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Doctor_' . date('YmdHis');
    }
}
