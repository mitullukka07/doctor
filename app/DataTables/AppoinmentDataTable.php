<?php

namespace App\DataTables;

use App\Models\Appoinment;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AppoinmentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $btn = "";
                $btn = '<a href="'.route('admin.show',$data->id).'" data-id="' . $data->id . '"  data-bs-toggle="modal" data-bs-target="#myModalshowappoinment"  class="edit btn btn-primary btn-sm show"><i class="fa fa-eye"></i></a>&nbsp ';                
                return $btn;
            })
            ->editColumn('doctor', function ($data) {
                return $data->doctor_name->name;
            })
            ->rawColumns(['action', 'doctor'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Appoinment $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Appoinment $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('appoinment-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           
            Column::make('NO')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('doctor')->title('DoctorName'),
            Column::make('start_time'),
            Column::make('end_time'),
            Column::make('datetime'),
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Appoinment_' . date('YmdHis');
    }
}
